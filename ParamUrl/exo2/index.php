<!DOCTYPE html>
<html lang="fr">

<?php $pageTitle = "Welcome home !"; include "header.php"; ?>

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title><?php echo $pageTitle; ?></title>
<link src="style.css" />
</head>
<body>
<h1>Welcome to my awesome Site</h1>

<p> nom= <?php
$nom =  $_GET['nom'];
if ($nom) {
    echo 'si le nom s\'affiche c\'est que le test a fonctionné --> '.$nom;    
}
?>
</p>

<p> prenom= <?php
$prenom =  $_GET['prenom'];
if ($prenom) {
    echo 'si le prenom s\'affiche c\'est que le test a fonctionné --> '.$prenom;    
}
?>
</p>

<p> age= <?php
$age =  $_GET['age'];
if ($age) {
    echo 'si l\'age s\'affiche c\'est que le test a fonctionné --> '.$age;    
}
else {
    echo 'la récupération de la donnée AGE n\' pas fonctionnée.';
}
?>
</p>


<?php include "footer.php";  ?>

</body>

</html>